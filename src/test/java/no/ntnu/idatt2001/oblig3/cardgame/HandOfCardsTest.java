package no.ntnu.idatt2001.oblig3.cardgame;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class HandOfCardsTest {

    static PlayingCard H4 = new PlayingCard('H', 4);
    static PlayingCard H1 = new PlayingCard('H', 1);
    static PlayingCard H6 = new PlayingCard('H', 6);
    static PlayingCard H5 = new PlayingCard('H', 5);
    static PlayingCard H9 = new PlayingCard('H', 9);

    static PlayingCard S5 = new PlayingCard('S', 5);
    static PlayingCard C12 = new PlayingCard('C', 12);

    static PlayingCard spareQueen = new PlayingCard('S', 12);

    @Test
    void size() {
    }

    @Test
    void isFlush() {
        ArrayList<PlayingCard> cards = new ArrayList<>();
        cards.add(H4);
        cards.add(H1);
        cards.add(H6);
        cards.add(H5);
        cards.add(H9);
        HandOfCards hand = new HandOfCards(cards);

        assertTrue(hand.isFlush());

    }

    @Test
    void isNotFlush() {
        ArrayList<PlayingCard> cards = new ArrayList<>();
        cards.add(S5);
        cards.add(H1);
        cards.add(H6);
        cards.add(H5);
        cards.add(H9);
        HandOfCards hand = new HandOfCards(cards);

        assertFalse(hand.isFlush());
    }

    @Test
    void getSumOnHand() {
        ArrayList<PlayingCard> cards = new ArrayList<>();
        cards.add(S5);
        cards.add(H1);
        cards.add(H6);
        cards.add(H5);
        cards.add(C12);
        HandOfCards hand = new HandOfCards(cards);

        //5 + 1 + 6 + 5 + 12 = 29

        assertEquals(29, hand.getSumOnHand());
    }

    @Test
    void containsSpareQueen() {
        ArrayList<PlayingCard> cards = new ArrayList<>();
        cards.add(S5);
        cards.add(H1);
        cards.add(H6);
        cards.add(spareQueen);
        cards.add(C12);
        HandOfCards hand = new HandOfCards(cards);

        assertTrue(hand.containsSpareQueen());
    }

    @Test
    void doesNotcontainSpareQueen() {
        ArrayList<PlayingCard> cards = new ArrayList<>();
        cards.add(S5);
        cards.add(H1);
        cards.add(H6);
        cards.add(H9);
        cards.add(C12);
        HandOfCards hand = new HandOfCards(cards);

        assertFalse(hand.containsSpareQueen());
    }
}