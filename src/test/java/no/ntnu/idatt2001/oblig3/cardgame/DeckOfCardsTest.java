package no.ntnu.idatt2001.oblig3.cardgame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {

    static DeckOfCards cards;

    @BeforeEach
    void initialize() {
        cards = new DeckOfCards();
    }

    @Test
    void dealHand() {
        HandOfCards hand = cards.dealHand(5);

        assertEquals(47, cards.cardsRemaining());
        assertEquals(5, hand.size());
    }

    @Test
    void sumOfDeck() {
        HandOfCards hand = cards.dealHand(52);

        //Sum of the whole deck: (Sigma(1...13) * 4) = 364
        assertEquals(364, hand.getSumOnHand());
    }

}