package no.ntnu.idatt2001.oblig3.cardgame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayingCardTest {

    @Test
    void testPlayingCard() {
        assertThrows(IllegalArgumentException.class, () -> {
            new PlayingCard('H', 17);
        });
    }

    @Test
    void getAsString() {
        PlayingCard c = new PlayingCard('H', 4);
        assertEquals("H4", c.getAsString());
    }

    @Test
    void getSuit() {
        PlayingCard c = new PlayingCard('S', 7);
        assertEquals('S', c.getSuit());
    }

    @Test
    void getFace() {
        PlayingCard c = new PlayingCard('D', 12);
        assertEquals(12, c.getFace());
    }

    @Test
    void testEquals() {
        PlayingCard c1 = new PlayingCard('H', 12);
        PlayingCard c2 = new PlayingCard('H', 12);

        assertEquals(c1, c2);

        PlayingCard c3 = new PlayingCard('D', 12);
        PlayingCard c4 = new PlayingCard('H', 12);

        assertNotEquals(c3, c4);
    }
}