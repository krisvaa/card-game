package no.ntnu.idatt2001.oblig3.cardgame;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

public class FrameController {

    /**
     * Variables to keep track of card deck, and card hand to be visualized
     */
    private DeckOfCards deck;
    private HandOfCards hand;

    /**
     * List to keep all VBoxes containing visualization of cards
     */
    private ArrayList<VBox> handCardContainers;
    @FXML
    public VBox handCard1;
    @FXML
    public VBox handCard2;
    @FXML
    public VBox handCard3;
    @FXML
    public VBox handCard4;
    @FXML
    public VBox handCard5;



    @FXML
    public Button dealHand;
    @FXML
    public Button checkHand;

    /**
     * Visual TextFields used to update user on given combination.
     */
    @FXML
    public TextField flush;
    @FXML
    public TextField queenOfSpades;
    @FXML
    public TextField sumOfFaces;
    @FXML
    public TextField cardsOfHeart;


    public FrameController() {
        refillDeck();
    }

    @FXML
    public void initialize() {
        handCardContainers = new ArrayList<>();
        handCardContainers.add(handCard1);
        handCardContainers.add(handCard2);
        handCardContainers.add(handCard3);
        handCardContainers.add(handCard4);
        handCardContainers.add(handCard5);

        dealHand();
        checkHand();
    }

    /**
     * Method used by "Deal hand" button
     *
     * Draws a new hand from deck, and refhreshes the visualisation of the hand.
     * If deck is near empty, the user will be notified, and a new deck will be created.
     */
    @FXML
    public void dealHand() {
        if(deck.cardsRemaining() < 5) {
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle("Oh no... :(");
            a.setContentText("No more cards in deck, press OK to refill.");
            a.show();

            refillDeck();
        }

        hand = deck.dealHand(5);
        refreshHand();
    }

    /**
     * Method runs when the user presses "Check Hand" button
     *
     * Checks the various combinations, and updates the representative visual label-box
     */
    @FXML
    public void checkHand() {
        checkFlush();
        checkSumOfFaces();
        checkCardsOfHeart();
        checkQueenOfSpades();
    }

    /**
     * Checks if current hand has a full five-flush, and updates the flush-label on screen.
     */
    private void checkFlush() {
        boolean isFlush = hand.isFlush();

        if(isFlush) {
            flush.setText("YES");
        } else {
            flush.setText("NO");
        }
    }

    /**
     * Gets current sum of faces on current hand, and updates the sum-label on screen.
     */
    private void checkSumOfFaces() {
        int sum = hand.getSumOnHand();
        sumOfFaces.setText("" + sum);
    }

    /**
     * Gets all cards on hand with Heart as suit, and updates the cards of heart-label on screen.
     */
    private void checkCardsOfHeart() {
        cardsOfHeart.setText("");
        hand.getCards().stream().filter(c -> c.getSuit() == 'H').
                forEach(h -> {
                    String s = cardsOfHeart.getText();
                    cardsOfHeart.setText(s + String.format("%s%d  ", h.getSuit(), h.getFace()));
                });

        if(cardsOfHeart.getText().equals("")) {
            cardsOfHeart.setText("No hearts on hand...");
        }
    }

    /**
     * Checks if there exists a queen of spade (Suit S, and number 12), and updates the queen of spades label on screen.
     */
    private void checkQueenOfSpades() {
        boolean containsSpadeQueen = hand.containsSpareQueen();

        if(containsSpadeQueen) {
            queenOfSpades.setText("YES");
        } else {
            queenOfSpades.setText("NO");
        }
    }

    /**
     * Refreshes the visual cards on screen, with information on from current hand-variable.
     *
     * Updates font-color, face and suit name of each card.
     */
    private void refreshHand() {
        for(int i = 0; i < handCardContainers.size(); i++) {
            PlayingCard card = hand.getCards().get(i);
            VBox cardContainer = handCardContainers.get(i);

            char suit = card.getSuit();
            int face = card.getFace();

            final String suitName;
            final String faceName;
            final String color;

            //Finds correct color, based on suit
            switch (suit) {
                case 'H':
                    suitName = "Heart";
                    color = "#FF0000"; //RED
                    break;
                case 'S':
                    suitName = "Spade";
                    color = "#000000"; //BLACK
                    break;
                case 'D':
                    suitName = "Diamonds";
                    color = "#FF0000"; //RED
                    break;
                case 'C':
                    suitName = "Clubs";
                    color = "#000000"; //BLACK
                    break;
                default:
                    suitName = "Unknown";
                    color = "#000000"; //BLACK
            }

            //Finds special character for face, or sets number of there exists no special character
            switch (face) {
                case 1: //Ace
                    faceName = "A";
                    break;
                case 11: //Knight
                    faceName = "J";
                    break;
                case 12: //Queen
                    faceName = "Q";
                    break;
                case 13: //King
                    faceName = "K";
                    break;
                default:
                    faceName = "" + face;
            }

            //Sets text color on all elements on the card
            cardContainer.getChildren().forEach(l -> l.setStyle("-fx-text-fill:" + color));

            //Updates card-face label
            cardContainer.getChildren().stream().
                    filter(l -> l.getStyleClass().contains("card_face")).map(l -> (Label) l).forEach(l -> l.setText(faceName));

            //Updates card-suit label
            cardContainer.getChildren().stream().
                    filter(l -> l.getStyleClass().contains("card_suit")).map(l -> (Label) l).forEach(l -> l.setText(suitName));
        }

    }

    /**
     * Generates a new deck
     */
    private void refillDeck() {
        deck = new DeckOfCards();
    }

}
