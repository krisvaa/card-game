package no.ntnu.idatt2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Random;
import java.util.stream.IntStream;

public class DeckOfCards {

    private ArrayList<PlayingCard> cards;

    //Array used for filling the deck with correct suits
    private final char[] suit = { 'S', 'H', 'D', 'C' };

    /**
     * Class to represent one deck of playing cards (52 cards).
     * Containes four different suits: Hearts (H), Spades (S), Clubs (C) and Diamonds (D)
     *
     */

    public DeckOfCards() {
        cards = new ArrayList<>(52);
        fillDeckWithCards();
    }

    /**
     *
     * Method for filling the deck with correct cards, 13 of each suit, a total of 52 cards.
     *
     */
    private void fillDeckWithCards() {
        for(char suit : suit) {
            for(int j = 1; j <= 13; j++) {
                PlayingCard p = new PlayingCard(suit, j);
                cards.add(p);
            }
        }
    }

    /**
     *
     * @return Remaining number of cards in deck
     */
    public int cardsRemaining() {
        return cards.size();
    }



    /**
     * Draws a random card from the deck, and removes it from deck
     *
     * @return Random playing card, that now have been removed from deck.
     */
    private PlayingCard drawRandomCard() {
        int cardsRemaining = cardsRemaining();

        if(cardsRemaining < 1) {
            throw new IllegalStateException("Attempted to draw card from empty deck");
        }

        if(cardsRemaining == 1) {
            return cards.get(0);
        }

        Random r = new Random();

        int index = r.nextInt(cardsRemaining - 1);

        return cards.remove(index);
    }


    /**
     * Draws numberOfCards random cards from the deck, removes them from deck, and puts them in HandOfCards object.
     *
     * @param numberOfCards Number of cards to be drawn to the hand.
     * @return Hand with given number of cards, picked randomly from the deck.
     */
    public HandOfCards dealHand(int numberOfCards) {
        if(numberOfCards < 1 || numberOfCards > cardsRemaining()) {
            throw new IllegalArgumentException("Cannot draw " + numberOfCards + " cards from a deck with " + cardsRemaining() + " cards remaining");
        }

        ArrayList<PlayingCard> cardsForHand = new ArrayList<>(numberOfCards);

        IntStream.range(0, numberOfCards).forEach(i -> cardsForHand.add(drawRandomCard()));

        return new HandOfCards(cardsForHand);
    }

    @Override
    public String toString() {
        return "DeckOfCards{" +
                "cards=" + cards +
                '}';
    }
}
