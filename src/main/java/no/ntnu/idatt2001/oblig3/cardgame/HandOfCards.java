package no.ntnu.idatt2001.oblig3.cardgame;

import java.util.ArrayList;

public class HandOfCards {

    private final ArrayList<PlayingCard> cards;

    /**
     * Immutable class to represent a hand of cards.
     *
     * @param cards Cards to be included in the given hand
     */
    public HandOfCards(ArrayList<PlayingCard> cards) {
        this.cards = cards;
    }

    /**
     *
     * @return The list of cards on hand
     */
    public ArrayList<PlayingCard> getCards() {
        return cards;
    }

    /**
     * @return Number of cards on hand
     */
    public int size() {
        return cards.size();
    }

    /**
     * Check if all cards have the same suit
     *
     * @return If all cards on hand has the same suit
     */
    public boolean isFlush() {
        return cards.stream().filter(l -> l.getSuit() == cards.get(0).getSuit()).count() == cards.size();
    }

    /**
     *
     * @return Returns the sum of faces on hand
     */
    public int getSumOnHand() {
        return cards.stream().mapToInt(c -> c.getFace()).sum();
    }

    /**
     *
     * @return True if the hand contains a spare Queen
     */
    public boolean containsSpareQueen() {
        PlayingCard spadeQueen = new PlayingCard('S', 12);
        return cards.contains(spadeQueen);
    }

    @Override
    public String toString() {
        return "HandOfCards{" +
                "cards=" + cards +
                '}';
    }
}
